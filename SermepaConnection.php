<?php


class SermepaConnection{

  private $transactionParams;

  // Public api

  public function setTransactionParam( $paramName, $value ){
    $this->transactionParams[ $paramName] = $value ;
  }

  public function getRequestData(){
    return base64_encode( json_encode($this->transactionParams) );
  }

  public function getSignature( $encryptionKey ){
    $key = base64_decode($encryptionKey);
    $dsMerchantParameters = $this->getRequestData();
    $newKey = $this->encryptOrderCode( $key, $this->getOrderCode() );
    $dsMerchanSignature = hash_hmac('sha256', $dsMerchantParameters, $newKey, true);
    return base64_encode($dsMerchanSignature);
  }

  public function getResponseSignature( $encryptionKey, $responseData ){
    $key = base64_decode($encryptionKey);
    $dsMerchantParameters = $this->decodeResponse( $responseData );
    $this->transactionParams = json_decode($dsMerchantParameters, true);
    $newKey = $this->encryptOrderCode( $key, $this->getOrderFromResponse() );
    $dsMerchanSignature = hash_hmac('sha256', $responseData, $newKey, true);
    return strtr(base64_encode($dsMerchanSignature), '+/', '-_');
  }

  public function decodeResponse( $responseData ){
    return base64_decode(strtr($responseData, '-_', '+/'));
  }


  // Helper Functions

  private function getOrderCode(){
    if( empty($this->transactionParams['DS_MERCHANT_ORDER']) ){
      return $this->transactionParams['Ds_Merchant_Order'];
    } else {
      return $this->transactionParams['DS_MERCHANT_ORDER'];
    }
  }

  private function getOrderFromResponse(){
    if( empty($this->transactionParams['Ds_Order']) ){
			return $this->transactionParams['DS_ORDER'];
		} else {
			return $this->transactionParams['Ds_Order'];
		}
  }

  private function encryptOrderCode( $key, $orderCode ){
    $iv = "\0\0\0\0\0\0\0\0";
    $data_padded = $orderCode;
    if (strlen($data_padded) % 8) {
        $data_padded = str_pad($data_padded, strlen($data_padded) + 8 - strlen($data_padded) % 8, "\0");
    }
    return openssl_encrypt($data_padded, "DES-EDE3-CBC", $key, OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
  }

}
